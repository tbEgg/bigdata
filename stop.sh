#! /usr/bin/bash

currentPath=$(pwd)

# 关闭各集群函数
# $1:集群名称，如spark，hadoop
# $2:集群启动后显示进程名称，如spark的Master
stop()
{
    ok=$(jps | grep "[^a-zA-Z]$2$")
    if [[  -z "$ok" ]]; then
        echo "$1未开启"
    else
        ./stop-all.sh
        sleep 1
        ok=$(jps | grep "[^a-zA-Z]$2$")
        if [[ ! -z "$ok" ]]; then
            echo "ok = $ok"
            echo "$1无法关闭，请自行解决"
            cd $currentPath
            exit
        fi 
        echo "$1成功关闭"
    fi
}

echo -n "是否希望关闭spark(y/n)："
read cmd
if [[ $cmd == "y" ]]; then
    cd ${SPARK_HOME}/sbin/
    stop spark Master
fi
echo -n "是否希望关闭hadoop(y/n)："
read cmd
if [[ $cmd == "y" ]]; then
    cd ${HADOOP_HOME}/sbin/
    stop hadoop NameNode
fi
cd $currentPath
