#! /usr/bin/bash
echo "<!--在使用该脚本前，请先阅读README文件-->"

# 定义各需要配置的文件的路径
declare HOST="/etc/hosts"
declare HADOOPCONF="${HADOOP_HOME}/etc/hadoop/slaves"
declare SPARKCONF="${SPARK_HOME}/conf/slaves"
declare HADOOPSITE="${HADOOP_HOME}/etc/hadoop/hdfs-site.xml"

declare hadoop="yes"
declare spark="yes"

# 支持hadoop3.1.1版本
if [[ ! -f $HADOOPCONF ]]; then
    HADOOPCONF="${HADOOP_HOME}/etc/hadoop/workers"
fi
if [[ ! -f $HADOOPCONF ]]; then
    echo "文件$HADOOPCONF不存在，路径错误或没有安装HADOOP"
    hadoop="no"
fi
if [[ ! -f $SPARKCONF ]]; then
    echo "文件$SPARKCONF不存在，路径错误或没有安装SPARK"
    spark="no"
fi
if [[ $hadoop == "no" ]] && [[ $spark == "no" ]]; then
    echo "什么都没有安装，先安装吧！"
    exit
fi
# 先不管hbase这玩意

# 根据/etc/hosts文件修改slaves文件
# $1:对应集群文件路径
sync()
{
    echo -n "" > $1
    cat $HOST | grep "^\([0-9]\{1,3\}\.\)\{3\}[0-9]\{1,3\}" | while read line
    do
        host=$(echo $line | cut -d " " -f 2)
        echo $host >> $1
    done
    master=$(hostname)
    sed -i "/$master$/d" $1 # 不包括Master
}

# $1:集群的slaves文件路径
send()
{
    cat $1 | while read host
    do
        scp $HOST $host:$HOST
        scp $1 $host:$1
    done
}

declare DATA="<value>file:\/usr\/local\/hadoop\/tmp\/dfs\/data"

# 调整hdfs-site.xml文件
# $1:hdfs-site.xml文件的位置
modify_hadoop()
{
    # 应该把其他文件也调整好，不管了
    index=1
    cp $1 "tmp_hadoop"
    cat $HADOOPCONF | while read host
    do
        index=$(($index + 1))
        sed -i "s/${DATA}[0-9]*<\/value>/${DATA}${index}<\/value>/g" "tmp_hadoop"
        scp "tmp_hadoop" $host:$1
    done
    rm "tmp_hadoop"
}

known="/root/.ssh/known_hosts"
echo -n "是否希望将$known文件删去(y/n)："
read cmd
if [[ $cmd == "y"  ]] && [[ -f $known ]]; then
    cat $known
    rm $known
    echo "known_hosts文件已被删除!"
fi

# 直接使用vim编辑
vim $HOST
echo -e "\ta:只需配置spark\n\tb:只需配置hadoop\n\tc:All"
echo -n "请输入选项："
read opt
case $opt in
    "a" )
        sync $SPARKCONF
        send $SPARKCONF
        ;;
    "b" )
        sync $HADOOPCONF
        send $HADOOPCONF
        modify_hadoop $HADOOPSITE
        ;;
    "c" )
        sync $SPARKCONF
        sync $HADOOPCONF
        send $SPARKCONF
        send $HADOOPCONF
        modify_hadoop $HADOOPSITE
        ;;
    * )
        echo "未知命令"
        exit
        ;;
esac
echo "传输成功！"
