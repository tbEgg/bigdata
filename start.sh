#! /usr/bin/bash

currentPath=$(pwd)

# 启动各集群函数
# $1:集群名称，如spark，hadoop
# $2:集群启动后显示进程名称，如spark的Master
start()
{
    ok=$(jps | grep "[^a-zA-Z]$2$")
    if [[ ! -z "$ok" ]]; then
        echo "$1正在运行，现在将其关闭"
        ./stop-all.sh
        sleep 1 # spark可能关的比较慢？

        ok=$(jps | grep "[^a-zA-Z]$2$")
        if [[ ! -z "$ok" ]]; then
            echo "$1无法关闭，请自行解决"
            cd $currentPath
            exit
        fi
        echo "$1已成功关闭，现在重启"
    fi

    ./start-all.sh
    ok=$(jps | grep "[^a-zA-Z]$2$")
    if [[ -z "$ok" ]]; then
        echo "启动$1集群失败"
        return 0
    else
        echo "启动成功"
        return 1
    fi
}

echo -n "是否希望启动spark(y/n)："
read cmd
if [[ $cmd == "y" ]]; then
    cd ${SPARK_HOME}/sbin/
    start spark Master
fi
echo -n "是否希望启动hadoop(y/n)："
read cmd
if [[ $cmd == "y" ]]; then
    cd ${HADOOP_HOME}/sbin/
    start hadoop NameNode
    if [[ $? == 1 ]]; then
        hdfs dfsadmin -safemode leave
    fi
fi
cd $currentPath
